function ajustar(){
    $('#mapContainer').height($(document).height()-$('#cabecera').height()-$('#botones').height());   
}

ajustar();

$( window ).resize(function() {
  $('#mapContainer').height($(document).height()-$('#cabecera').height()-$('#botones').height());   
});
            
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        console.log('Received Event: ' + id);
    }
    
};

/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "35%";
    //document.getElementById("main").style.marginLeft = "250px";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    //document.getElementById("main").style.marginLeft = "0";
}

$("#onboard_logo").click(function(){
    $("#easter_egg").modal("toggle");
});

$("#cerrar_easteregg").click(function(){
    $("#easter_egg").modal("toggle");
});

app.initialize();