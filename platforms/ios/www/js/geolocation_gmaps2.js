var map;
var marker;
var mapa_creado = false;
crear_mapa();
var origenGlobal;
var destinoGlobal;
var directionsService = new google.maps.DirectionsService();
var directionsDisplay = new google.maps.DirectionsRenderer();


function crear_mapa(){
	console.log('crea mapa');
	var myLatlng = new google.maps.LatLng(-33.462442,-70.598334);
	var mapOptions = {zoom: 14,center: myLatlng}
	map = new google.maps.Map(document.getElementById('map'), mapOptions);;
	marker = new google.maps.Marker({position: myLatlng,map: map});
	mapa_creado = true;		
}

navigator.geolocation.getCurrentPosition(onSuccess, onError, { timeout: 30000 });


function centrar(lat,lon){
	
	var centro = new google.maps.LatLng(lat,lon);
	marker.setPosition(centro);
	map.panTo(centro);
	
	var destino = new google.maps.LatLng(-33.462442,-70.598334);
	destinoGlobal = destino;
	origenGlobal = centro;
	directionsDisplay.setMap(map);
}

function onSuccess(position) {
	var lat=position.coords.latitude;
	var lon=position.coords.longitude;
	console.log(lat+','+lon)
	if(mapa_creado){
		centrar(lat,lon);
	}
}

function onError(error) {
  alert('code: ' + error.code + '\n' +'message: ' + error.message + '\n');
}

// Genera Ruta
$("#ruta").click(function(){
	calcRoute(origenGlobal,destinoGlobal);
});

function calcRoute(origen, destino) {
	var args = {
		origin: origen,
		destination: destino,
		travelMode: google.maps.TravelMode.DRIVING
	}
	directionsService.route(args, function (response, status) {
		if (status==google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
			var myroute = directionsDisplay.directions.routes[0];
			console.log(myroute);
			var distance = 0;
			for (i = 0; i < myroute.legs.length; i++) {
				distance += myroute.legs[i].distance.value;
				//for each 'leg'(route between two waypoints) we get the distance and add it to the total
			}
			console.log(distance +" metros");
		} else {
			alert("fail");
		}
	});
};

