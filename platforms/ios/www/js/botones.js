var datos_movil;
var datos_post;
var estado;
// crea boton
//Botones
$("#132").click(function() {
    llama_bomberos();
});

$("#131").click(function() {
    llama_ambulancia();
});

function add(type) {
    var element = document.createElement("button");
    element.type = "button";
    element.name = type;
    element.id = type;
    var nuevaDiv = document.getElementById("botones");
    nuevaDiv.appendChild(element);
}
// modifica los botones
function crear_botones() {
    $("#identificador").html(datos_movil.titulo + " " + datos_movil.nombre_dispositivo);
    alert("Cargando botones " + datos_movil.nombre_dispositivo);
    $("#132").hide();
    $("#131").hide();
    $.each(datos_movil.botones, function(index, value) {
        add(value.id);
        $("#" + value.id).html(value.titulo);
        $("#" + value.id).addClass("btn btn-default");
        $("#" + value.id).css("color", value.color);
        $("#" + value.id).click(function() {
            estado = value.descripcion;
            //alert(estado);
            var fechaYHora = Date();
            datos_post = {"estado":estado, "lat":globalLat, "lon":globalLon, "fecha y hora":fechaYHora};
            enviar_post(datos_post);
            //alert("obteniendo posicion");
        });
    })
};

function enviar_post(outData) {
    console.log('datos',outData);
    $.ajax({
        data: outData,
        url: "http://www.exefire.com/log/index.php",
        dataType: "jsonp",
        async: true,
        success: function(result) {
            console.log(result);
           //alert("enviado");
        },
        error: function(request, error) {
            var texto = "Error conectando con el servidor!";
            alert(texto);
        }
    });
}
// AJAX 
function llama_bomberos() {
    $.ajax({
        url: "http://panel.firesoft.cl/onboard/json.php?id_vehiculo=1",
        dataType: "jsonp",
        async: true,
        success: function(result) {
            console.log(result);
            datos_movil = result;
            crear_botones();
        },
        error: function(request, error) {
            var texto = "Error conectando con el servidor!";
            alert(texto);
        }
    });
}
//AJAX
function llama_ambulancia() {
    $.ajax({
        url: "http://panel.firesoft.cl/onboard/json.php?id_vehiculo=3",
        dataType: "jsonp",
        async: true,
        success: function(result) {
            console.log(result);
            datos_movil = result;
            crear_botones();
        },
        error: function(request, error) {
            var texto = "Error conectando con el servidor!";
            alert(texto);
        }
    });
}