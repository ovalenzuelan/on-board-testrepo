var map;
var headingActual;
// ..Coordenadas..
var globalLat; //
var globalLon; //Latitud y longitud en uso
var nuevaLat // Lat y Long obternidas
var nuevaLon;//
// .............
var myroute;
var dist;
var rutaArray;
var encodedPolyline;
var marker;
var maxAge = 0;
var mapa_creado = false;
var anteriorPos;
var actualPos;
var centro;
var directionsService = new google.maps.DirectionsService();
var directionsDisplay = new google.maps.DirectionsRenderer();
var LatLngAlarma;
var llamado = {"fechaHora":"2017-01-10 09:12:00", //Formato JSON Despacho
			   "clave":"10-0, CC",
			   "direccion":"Las Encinas",
			   "esquina":"Jose Pedro Alessandri",
			   "comuna":"Ñuñoa",
			   "sector":"28",
			   "LatLng":"-33.4690735373722,-70.5985342987396",
			}
function crear_mapa(){
	console.log('crea mapa');
	var myLatlng = new google.maps.LatLng(-33.462442,-70.598334);
	var mapOptions = {zoom: 14,center: myLatlng}
	map = new google.maps.Map(document.getElementById('map'), mapOptions);;
	marker = new google.maps.Marker({position: myLatlng,map: map});
	mapa_creado = true;		
}
// --------------->
function rotar(cuanto){
	rotation += cuanto;
	$('#map').css({'transform' : 'rotate('+ rotation +'deg)'});
};
function cambiar_heading(grados){
	$('#map').css({'transform' : 'rotate('+ grados +'deg)'});
};
function panear_mapa(lat,lon,head){
	cambiar_heading(head);
}
function heading(punto1,punto2){
	var p2 = {
		x: punto1.lat,
		y: punto1.lon
	};
	var p1 = {
		x: punto2.lat,
		y: punto2.lon
	};
	var angleRadians = Math.atan2(p2.y - p1.y, p2.x - p1.x);
	var angleDeg = Math.atan2(p2.y - p1.y, p2.x - p1.x) * 180 / Math.PI;
	headingActual = Math.round((angleDeg*100)/100);
	console.log(headingActual);
}

function rad2deg(rad){
  return rad * (180/Math.PI);
}

function deg2rad(deg) {
  return deg * (Math.PI/180);
}
function calcula_distancia(lat1, lon1, lat2, lon2) {
	if(!(lat1==lat2 && lon1==lon2)){
		var radlat1 = Math.PI * lat1/180;
		var radlat2 = Math.PI * lat2/180;
		var theta = lon1-lon2;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515 * 1.609344 * 1000;
		return dist;
	}
	console.log(dist);
	return null;
}
// --------------------------------->
$(document).ready(function(){
	crear_mapa();
	navigator.geolocation.getCurrentPosition(onSuccess,onError, geo_options);
	navigator.geolocation.watchPosition(watchSuccess, onError, geo_options);
	});

function onSuccess(position) {
	globalLat=position.coords.latitude;
	globalLon=position.coords.longitude;
	anteriorPos = {"lat":globalLat,"lon":globalLon};
	console.log(anteriorPos);
	centrar(globalLat,globalLon);
	maxAge = 15000;
	nuevoDespacho();	
}
function watchSuccess(position){
	nuevaLat = position.coords.latitude;
	nuevaLon = position.coords.longitude;
	console.log(position.coords.accuracy + " Accuracy");
	if(calcula_distancia(globalLat,globalLon,nuevaLat,nuevaLon)>position.coords.accuracy/3){
		actualPos = {"lat":nuevaLat,"lon":nuevaLon};	
			if (Math.round(bdccGeoDistanceToPolyMtrs(encodedPolyline, centro))<position.coords.accuracy/4) {
				centrar(nuevaLat, nuevaLon);
				heading(anteriorPos,actualPos);
				cambiar_heading(headingActual);	
				$("#contadorSuccess").val(parseInt($("#contadorSuccess").val())+1);						
			}else{
				window.alert("Recalculando Ruta");
				encodedPolyline.setMap(null);
				headingActual = 0;
				centrar(nuevaLat,nuevaLon);
				recalcularDespacho();
			}
		globalLat = nuevaLat;
		globalLon = nuevaLon;
		anteriorPos = {"lat":globalLat,"lon":globalLon};
	}
}
function onError(error) {
  alert('code: ' + error.code + '\n' +'message: ' + error.message + '\n');
};
var geo_options = {
  enableHighAccuracy: true
};



function centrar(lat,lon){
	centro = new google.maps.LatLng(lat ,lon)
	marker.setPosition(centro);
	map.panTo(centro);
	//directionsDisplay.setMap(map);
	//directionsDisplay.setOptions()
}
function calcRoute(origen, destino) {
	var args = {
		origin: origen,
		destination: destino,
		travelMode: google.maps.TravelMode.DRIVING
	}
	directionsService.route(args, function (response, status) {
		if (status==google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
			myroute = directionsDisplay.directions.routes[0];
			console.log(myroute);
			var distance = 0;
			for (i = 0; i < myroute.legs.length; i++) {
				distance += myroute.legs[i].distance.value;
				//for each 'leg'(route between two waypoints) we get the distance and add it to the total
			}
			//console.log(distance +" metros");
		} else {
			console.log(status);
		}
		var rutaCodificada = myroute.overview_polyline;
		var rutaDecodificada = google.maps.geometry.encoding.decodePath(rutaCodificada);
		encodedPolyline = new google.maps.Polyline ( { 
              strokeColor: "#FF0000" , 
              strokeOpacity: 0.8 , 
              strokeWeight: 2 , 
              path: rutaDecodificada , 
              clickable: false 
		}); 
		var ruta = encodedPolyline.getPath().getArray();
		rutaArray = ruta.map(function(el){
			return {"lat":el.lat(), "lon":el.lng()};
		})
		console.log(rutaArray);
		encodedPolyline.setMap(map);
		map.setZoom(16);
			
		});
};

function nuevoDespacho() {
    $.ajax({
        url: "http://panel.firesoft.cl/onboard/json.php?id_vehiculo=1",
        dataType: "jsonp",
        async: true,
        success: function(result) {
        LatLngAlarma = parseInt(llamado.LatLng);
        console.log("Nuevo llamado " + llamado.clave + " " + llamado.direccion + " " + llamado.esquina);
	 	$("#llamado").html(llamado.clave + " " + llamado.direccion + " " + llamado.esquina + " Comuna: " + llamado.comuna);
		calcRoute(centro,llamado.LatLng);
        },
        error: function(request, error) {
            var texto = "Error conectando con el servidor!";
            alert(texto);
        }
    });
}
function recalcularDespacho() {
    $.ajax({
        url: "http://panel.firesoft.cl/onboard/json.php?id_vehiculo=1",
        dataType: "jsonp",
        async: true,
        success: function(result) {
        centrar(globalLat, globalLon);
        if (centro != llamado.LatLng) {	
        calcRoute(centro,llamado.LatLng);
    	}else{
    		directionsDisplay.setMap(null);
    	}
        },
        error: function(request, error) {
            var texto = "Error conectando con el servidor!";
            alert(texto);
        }
    });
};