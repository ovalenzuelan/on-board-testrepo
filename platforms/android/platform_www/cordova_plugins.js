cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "id": "cordova-plugin-geolocation.geolocation",
        "file": "plugins/cordova-plugin-geolocation/www/android/geolocation.js",
        "pluginId": "cordova-plugin-geolocation",
        "clobbers": [
            "navigator.geolocation"
        ]
    },
    {
        "id": "cordova-plugin-geolocation.PositionError",
        "file": "plugins/cordova-plugin-geolocation/www/PositionError.js",
        "pluginId": "cordova-plugin-geolocation",
        "runs": true
    },
    {
        "id": "cordova-plugin-locationservices.Coordinates",
        "file": "plugins/cordova-plugin-locationservices/www/Coordinates.js",
        "pluginId": "cordova-plugin-locationservices",
        "clobbers": [
            "cordova.plugins.locationServices.Coordinates",
            "plugin.locationServices.Coordinates"
        ]
    },
    {
        "id": "cordova-plugin-locationservices.PositionError",
        "file": "plugins/cordova-plugin-locationservices/www/PositionError.js",
        "pluginId": "cordova-plugin-locationservices",
        "clobbers": [
            "cordova.plugins.locationServices.PositionError",
            "plugin.locationServices.PositionError"
        ]
    },
    {
        "id": "cordova-plugin-locationservices.Position",
        "file": "plugins/cordova-plugin-locationservices/www/Position.js",
        "pluginId": "cordova-plugin-locationservices",
        "clobbers": [
            "cordova.plugins.locationServices.PositionError",
            "plugin.locationServices.PositionError"
        ]
    },
    {
        "id": "cordova-plugin-locationservices.LocationServices",
        "file": "plugins/cordova-plugin-locationservices/www/LocationServices.js",
        "pluginId": "cordova-plugin-locationservices",
        "clobbers": [
            "cordova.plugins.locationServices.geolocation",
            "plugin.locationServices.geolocation"
        ]
    },
    {
        "id": "cordova-plugin-firebase.FirebasePlugin",
        "file": "plugins/cordova-plugin-firebase/www/firebase.js",
        "pluginId": "cordova-plugin-firebase",
        "clobbers": [
            "FirebasePlugin"
        ]
    },
    {
        "id": "cordova-plugin-splashscreen.SplashScreen",
        "file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
        "pluginId": "cordova-plugin-splashscreen",
        "clobbers": [
            "navigator.splashscreen"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.3.1",
    "cordova-plugin-compat": "1.1.0",
    "cordova-plugin-geolocation": "2.4.1",
    "cordova-plugin-locationservices": "2.1.0",
    "cordova-plugin-firebase": "0.1.19",
    "cordova-plugin-websql": "0.0.10",
    "cordova-plugin-splashscreen": "4.0.3-dev"
};
// BOTTOM OF METADATA
});