var map;
var globalLat; 
var globalLon; 
var nuevaLat 
var nuevaLon;
var myroute;
var encodedPolyline;
var marker;
var mapa_creado = false;
var anteriorPos;
var actualPos;
var centro;
var directionsService = new google.maps.DirectionsService();
var directionsDisplay = new google.maps.DirectionsRenderer({preserveViewport: true, suppressMarkers: true});
var geo_options = { enableHighAccuracy : true };
var waypoints = [];
var waypointsMarkers = [];
var latlonAlarma;
var waypoint_temporal = [];
var waypointsMarkers_temporal = [];

function crear_mapa(){
	console.log('crea mapa');
	var myLatlng = new google.maps.LatLng(-33.462442,-70.598334);
	var mapOptions = {zoom: 16,center: myLatlng, mapTypeId: google.maps.MapTypeId.ROADMAP,}
	map = new google.maps.Map(document.getElementById('map'), mapOptions);;
	marker = new google.maps.Marker({position: myLatlng, map: map, animation: google.maps.Animation.DROP });
	mapa_creado = true;		
};

function onSuccess(position) {
	globalLat=position.coords.latitude;
	globalLon=position.coords.longitude;
	anteriorPos = {"lat":globalLat,"lon":globalLon};
	centrar(anteriorPos.lat,anteriorPos.lon);
	maxAge = 15000;
	}

function watchSuccess(position){
	nuevaLat = position.coords.latitude;
	nuevaLon = position.coords.longitude;
	console.log(position.coords.accuracy);
	if(calcula_distancia(globalLat,globalLon,nuevaLat,nuevaLon)>position.coords.accuracy){
		actualPos = {"lat":nuevaLat,"lon":nuevaLon};
		centrar(actualPos.lat, actualPos.lon);	
			if (bdccGeoDistanceToPolyMtrs(encodedPolyline, centro)<position.coords.accuracy) {
				cambiar_heading(parseInt(position.coords.heading*-1));						
			}else{
				cambiar_heading(parseInt(position.coords.heading*-1));
				recalcularDespacho();
			}
		globalLat = nuevaLat;
		globalLon = nuevaLon;
		anteriorPos = {"lat":globalLat,"lon":globalLon};
	}
}

function onError(error) {
  console.log('code: ' + error.code + '\n' +'message: ' + error.message + '\n');
  alert("GPS no disponible");
  mapBackup = window.localStorage.getItem("map");
  mapOptionsBackup = window.localStorage.getItem("mapOptions");
  
};

$(document).ready(function(){
	verifyToken();
	crear_mapa();
	var getPosition = navigator.geolocation.getCurrentPosition(onSuccess,onError, geo_options);
	var watchPosition = navigator.geolocation.watchPosition(watchSuccess, onError, geo_options);
	llamaBomberos();
	display_c();
});

function centrar(lat,lon){
	centro = new google.maps.LatLng(lat ,lon)
	marker.setPosition(centro);
	map.panTo(centro);	
}

function recalcularDespacho(){
	if (waypoint_temporal.length>1) {
		calcRoute(centro,latlonAlarma,waypoint_temporal);
	}else{
		calcRoute(centro,latlonAlarma);
	}
}

function calcRoute(origen, destino, waypoint) {
	var args = {
		origin: origen,
		destination: destino,
		travelMode: google.maps.TravelMode.DRIVING,
		waypoints: waypoint,
		optimizeWaypoints: false,
		provideRouteAlternatives: false,
	}
	directionsService.route(args, function (response, status) {
		if (status==google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
			directionsDisplay.setMap(map);
			myroute = directionsDisplay.directions.routes[0];
			console.log(myroute.legs[0].steps[0].instructions);	
			var distance = 0;
		} else {
			console.log(status);
		}
		var rutaCodificada = myroute.overview_polyline;
		var rutaDecodificada = google.maps.geometry.encoding.decodePath(rutaCodificada);
		encodedPolyline = new google.maps.Polyline ({ 
              strokeColor: "#FF0000" , 
              strokeOpacity: 0.8 , 
              strokeWeight: 2 , 
              path: rutaDecodificada , 
              clickable: false 
		}); 
		var ruta = encodedPolyline.getPath().getArray();
		var	rutaArray = ruta.map(function(el){
			return {"lat":el.lat(), "lon":el.lng()};
			})			
		});
};

//FUNCIONALIDADES
//----------------------------------->

var gradosX = 0;
function cambiar_heading2(suma){
	gradosX = gradosX+suma;
	$('#map').css({'transform' :'rotate('+ gradosX +'deg)'});
};
function cambiar_heading(grados){
	$('#map').css({'transform' :'rotate('+ grados +'deg)'});
};
function rad2deg(rad){
  return rad * (180/Math.PI);
}
function deg2rad(deg) {
  return deg * (Math.PI/180);
}
function calcula_distancia(lat1, lon1, lat2, lon2) {
	if(!(lat1==lat2 && lon1==lon2)){
		var radlat1 = Math.PI * lat1/180;
		var radlat2 = Math.PI * lat2/180;
		var theta = lon1-lon2;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515 * 1.609344 * 1000;
		return dist;
	}else{
		return null;
	}
}
function msToKmh(ms){
	var kmH = ms * 3.6;
	return kmH;
};
// --------------------------------->

$("#centrar").click(function(){
	centrar(anteriorPos.lat, anteriorPos.lon);
	map.setZoom(17);
})

function removeTable(campos){
	var i = 1;
	$.each(campos, function(){
		$("#tr" + i).remove();
		i = i + 1;
	});
};

function addTable(campos, evento){
	removeTable(campos);
	var filas = ""; 
	var i = 1;
	$.each(campos, function(key, value){
		var destino_actual = $.grep(evento.destinos, function(e){ return e.destino_id == window.localStorage.getItem("destino_id"); });
		var valor_tabla = "";
		switch(key) {
		    case "fecha":
		        valor_tabla = evento.fecha;
		        break;
		    case "clave":
		        valor_tabla = destino_actual[0].clave;
		        break;
		    case "direccion":
		        valor_tabla = destino_actual[0].campos.direccion;
		        break;
		    case "esquina":
		        valor_tabla = destino_actual[0].campos.esquina;
		        break;
		    case "comuna":
		        valor_tabla = destino_actual[0].campos.comuna;
		        break; 
		    case "sector":
		        valor_tabla = destino_actual[0].campos.sector;
		        break;           
		    default:
		        valor_tabla = "N/A";
		}
		filas = "<tr id="+"tr"+i+">"+"<th scope='row'>"+value+"</th>"+"<td id="+key+">"+((valor_tabla!="")?valor_tabla:"N/A")+"</td>"+"</tr>";
		$("#infoTabla").append(filas);
		i = i+1;
	});
};
var contador_destino_id = 0;
function nuevoDespacho(alarma, waypointsMarkers){
	contador_destino_id = 0;
	var rawLlamados = window.localStorage.getItem("events");
    var llamados = JSON.parse(rawLlamados);
	var destinoFinal = alarma.destinos.length - 1;
	if (alarma.destinos.length > 1){  				//Alarmas con mas de un destino
		setMapOnAll(null, waypointsMarkers);
		waypoints = [];
		waypointsMarkers = [];
		directionsDisplay.setMap(null);
		$("#saltar").remove();
		$("#directo").remove();
		latlonAlarma = {"lat":parseFloat(alarma.destinos[destinoFinal].lat) , "lng":parseFloat(alarma.destinos[destinoFinal].lon)};
		$.each(alarma.destinos, function(){	
				var paradaLatLon = {"lat":parseFloat(this.lat),"lng":parseFloat(this.lon)};
				var parada = {location:paradaLatLon, stopover:true};
				var paradaMarker = new google.maps.Marker({position: paradaLatLon});
				waypointsMarkers.push(paradaMarker)
				waypoints.push(parada);
		});
		waypoint_temporal = waypoints;
		waypointsMarkers_temporal = waypointsMarkers;
		window.localStorage.setItem("waypointsMarkers" , waypointsMarkers);
    	window.localStorage.setItem("waypoints" , waypoints);
		$("#menu_destinos").remove();
		var saltar = document.createElement("button");
        saltar.type = "button";
        saltar.id = "saltar";
        var nuevaDiv = document.getElementById("extra_botones");
        nuevaDiv.appendChild(saltar);
        var directo = document.createElement("button");
        directo.type = "button";
        directo.id = "directo";
        nuevaDiv.appendChild(directo);
        $("#extra_botones").append("<ul id=menu_destinos class=dropdown-menu> </ul>");
        $.each(alarma.destinos , function(index, value){
        	var opcion = "<li id=dest><a href=# onclick=dir_a("+value.destino_id+")>"+value.clave+"</a></li>"
        	$("#menu_destinos").append(opcion);

        });
		$("#directo").attr("data-toggle", "dropdown").html("Ir directo a").css("color","#000").addClass("btn btn-info btn-sm col-xs-6 dropdown-toggle");
		$("#saltar").html("Saltar Destino").css("color","#000").addClass("btn btn-info btn-sm col-xs-6").click(function(){
			if (waypointsMarkers_temporal.length <= 1) {
    			removeTable(llamados.campos);
    			directionsDisplay.setMap(null);
				setMapOnAll(null, waypointsMarkers);
				$("#directo").remove();
				$("#saltar").remove();
				$("#infobar").html("");
				return;
			}
			if (waypointsMarkers_temporal.length == 2) {
				$("#saltar").html("Terminar Ruta");
				$("#directo").remove();
			}
			setMapOnAll(null, waypointsMarkers);
			contador_destino_id = contador_destino_id+1;
			var evento_actual = $.grep(llamados.eventos, function(e){ return e.evento_id == window.localStorage.getItem("evento_id"); });
			window.localStorage.setItem("destino_id", evento_actual[0].destinos[contador_destino_id-1].destino_id);
			var destino_actual = $.grep(evento_actual[0].destinos, function(e){ return e.destino_id == window.localStorage.getItem("destino_id"); });
			actualizar_tabla(destino_actual[0]);
			waypoint_temporal.shift();
			waypointsMarkers_temporal.shift();
			calcRoute(centro, latlonAlarma, waypoint_temporal);
			setMapOnAll(map, waypointsMarkers_temporal);
		});
		setMapOnAll(map, waypointsMarkers_temporal);
		calcRoute(centro, latlonAlarma, waypoint_temporal);
	}else{ 											// Alarmas con 1 destino
		setMapOnAll(null, waypointsMarkers);
		waypoints = [];
		waypointsMarkers = [];
		$("#directo").remove();
		$("#saltar").remove();
		var evento_actual = $.grep(llamados.eventos, function(e){ return e.evento_id == window.localStorage.getItem("evento_id"); });
		window.localStorage.setItem("destino_id", evento_actual[0].destinos[0].destino_id);
		latlonAlarma = {"lat":parseFloat(alarma.destinos[destinoFinal].lat) , "lng":parseFloat(alarma.destinos[destinoFinal].lon)};
		console.log(latlonAlarma);
		var destino = new google.maps.Marker({position: latlonAlarma});
		waypointsMarkers.push(destino);
		calcRoute(centro, latlonAlarma);
		setMapOnAll(map, waypointsMarkers);
		window.localStorage.setItem("waypointsMarkers" , waypointsMarkers);
    	window.localStorage.setItem("waypoints" , waypoints);
	}
    closeNav();
    return waypointsMarkers;
}


	
function dir_a(destino){
	setMapOnAll(null, waypointsMarkers);
	waypoint_temporal = [];
	waypointsMarkers_temporal = [];
	for (var i = 0; i < waypoints.length; i++) {
		waypoint_temporal.push(waypoints[i]);
		waypointsMarkers_temporal.push(waypointsMarkers[i]);
	}
	contador_destino_id = destino;
	console.log(waypoint_temporal);
	console.log(waypointsMarkers_temporal);
	directionsDisplay.setMap(null);
	console.log(destino);
	var llamados = JSON.parse(window.localStorage.getItem("events"));
	var evento_actual = $.grep(llamados.eventos, function(e){ return e.evento_id == window.localStorage.getItem("evento_id"); });
	var destino_directo = $.grep(evento_actual[0].destinos, function(e){ return e.destino_id == destino; });
	window.localStorage.setItem("destino_id" , destino);
	if (destino == evento_actual[0].destinos.length) {
		setMapOnAll(null, waypointsMarkers);
		for (var i = 1; i < evento_actual[0].destinos.length ; i++) {
			waypoint_temporal.shift();
			waypointsMarkers_temporal.shift();
		}
		actualizar_tabla(destino_directo[0]);
		var punto_final = {"lat": destino_directo[0].lat , "lng": destino_directo[0].lon};
		calcRoute(centro, punto_final);
		setMapOnAll(map, waypointsMarkers_temporal);
		$("#saltar").html("Terminar Ruta");
		$("#menu_destinos").remove();
		$("#directo").remove();
	}else{
		setMapOnAll(null, waypointsMarkers);
		actualizar_tabla(destino_directo[0]);
		for (var i = 1; i < destino; i++) {
			waypoint_temporal.shift();
			waypointsMarkers_temporal.shift();
		}
		var destino_final = evento_actual[0].destinos.length - 1;
		var latlonAlarma = {"lat":evento_actual[0].destinos[destino_final].lat , "lng":evento_actual[0].destinos[destino_final].lon};
		calcRoute(centro,latlonAlarma,waypoint_temporal);
		setMapOnAll(map, waypointsMarkers_temporal);
	}
}

function actualizar_tabla(destino){
	console.log(destino);
	$("#clave").html(destino.clave);
	$("#direccion").html(destino.campos.direccion);
	if (!destino.campos.esquina == "") {
		$("#esquina").html(destino.campos.esquina);
	}else{
		$("#esquina").html("N/A");
	}
	$("#comuna").html(destino.campos.comuna);
	$("#sector").html(destino.campos.sector);
}

function setMapOnAll(map, arreglo) {
  for (var i = 0; i < arreglo.length; i++) {
    arreglo[i].setMap(map);
  }
}

