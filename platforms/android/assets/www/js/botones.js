var datos_movil;
var mytime;
var input = document.getElementById("buscar");
var autocomplete = new google.maps.places.Autocomplete(input, options);
var options = {
    bounds: centro,
    types: ["address"]
};

function addButton(type) {
    var element = document.createElement("button");
    element.type = "button";
    element.id = type;
    var nuevaDiv = document.getElementById("botonera");
    nuevaDiv.appendChild(element);
}

function crear_botones() {
    $("#idVehiculo").html(datos_movil.nombre_dispositivo);
    $("#idVehiculo").addClass("text-uppercase text-center");
    console.log("Cargando botones " + datos_movil.nombre_dispositivo);
    $.each(datos_movil.botones, function(index, value) {
        addButton("btn" + value.num);
        $("#btn" + value.num).html(value.clave);
        if (value.pre == null) {
        $("#btn" + value.num).addClass("btn btn-primary btn-md col-xs-6 active");
        }else{
            $("#btn" + value.num).addClass("btn btn-primary btn-md col-xs-6 disabled");
        }
        $("#btn" + value.num).click(function() {
            $("#directo").remove();
            $("#saltar").remove();
            setMapOnAll(null , waypointsMarkers);
            var estado = value.texto;
            var fechaHora = Date();
            directionsDisplay.setMap(null);
            var datos_post = {"estado":estado, "lat":globalLat, "lon":globalLon, "fecha y hora":fechaHora};
            enviar_post(datos_post);
                $.each(value.desactiva ,function(index, value){
                    $("#btn" + value).addClass("disabled").removeClass("active");
                });
                $.each(value.activa ,function(index, value){
                    $("#btn" + value).removeClass("disabled").addClass("active");
                });
            
            if (datos_movil.botones.length == value.num) {
                var rawLlamados = window.localStorage.getItem("events");
                var llamados = JSON.parse(rawLlamados);
                $("#infobar").html("");
                removeTable(llamados.campos);
                setMapOnAll(null , waypointsMarkers);
            }
            
        });
    })
};

function enviar_post(outData) {
    $.ajax({
        type: "POST",
        url: "http://54.91.87.65/api_test_on_board/public/index.php/eventoBotones",
        data: outData,
        success: function(result) {
            console.log(result);
            return true;
                    },
        dataType: "JSON",
        error: function(request, error) {
            var texto = "Error conectando con el servidor!";
            console.log(error);
        }
    });
}

function buscarDireccion(){
    calcRoute(centro, document.getElementById("buscar").value);
    $("#buscar").val("");
}

$("#ocultar").click(function(){
    var clicked = $(this).data('clicked');
    if ( clicked ) {
        $("#capas").children().hide(500);
        $("#ocultar").html("mostrar").show(500);    
    }else{
        $("#capas").children().show(500);
        $("#ocultar").html("ocultar");
    }

    $(this).data('clicked', !clicked);
});

function llamaBomberos() {
    var data = window.localStorage.getItem("device_id");
    $.post({
        data: {"device_id":data},
        url: "http://54.91.87.65/api_test_on_board/public/index.php/config",
        
        async: true,
        success: function(result) {
            window.localStorage.setItem("config", result);
            datos_movil = JSON.parse(result);
            crear_botones();
            obtenerEventos();
        },
        error: function(request, error) {
            var texto = "Error conectando con el servidor!";
            console.log(error);
        }
    });
}


function layerButton(id){
    var clicked = $(this).data('clicked');
    
    if ( clicked ) {
        $("#" + id).addClass("disabled").removeClass("active");
        
    }else{
        $("#" + id).addClass("active").removeClass("disabled");
        
    }

    $(this).data('clicked', !clicked);
};

function obtenerEventos(){
    var device_id = window.localStorage.getItem("device_id");
    $.post({
        data: {"device_id":device_id },
        url: "http://54.91.87.65/api_test_on_board/public/index.php/events",
        async: true,
        success: function(result) {
            window.localStorage.setItem("events", result);
            cargarLlamados();
            var raw_config = window.localStorage.getItem("config");
            var config = JSON.parse(raw_config);
            var destino_id = window.localStorage.getItem("destino_id");
            console.log(config);
            obtenerCapas(destino_id ,config.capas);
        },
        error: function(request, error) {
            var texto = "Error conectando con el servidor!";
            console.log(error);
        }
    });
}

var waypointsMarkers = [];
function cargarLlamados(){
    var llamados = JSON.parse(window.localStorage.getItem("events")); //
    $.each(llamados.eventos, function(index, value){
        newEvent = $(document.createElement('a')).attr("id", "evento_id_" + value.evento_id).text(value.clave).click(function(){
            window.localStorage.setItem("destinos", value.destinos);
            window.localStorage.setItem("evento_id", value.evento_id);
            window.localStorage.setItem("destino_id", value.destinos[0].destino_id);
            addTable(llamados.campos, value);
            setMapOnAll(null, waypointsMarkers);
            waypointsMarkers = [];

            waypointsMarkers = nuevoDespacho(value, waypointsMarkers);
            if (value.destinos.length > 1) {
                $("#infobar").html(value.clave);
            }else{
                $("#infobar").html(value.clave + " " + value.destinos[0].campos.direccion + " / " + value.destinos[0].campos.esquina + " , " + value.destinos[0].campos.comuna);
            }
        });
        $("#mySidenav").append(newEvent);
    });
}

function obtenerCapas(destino, capa){
    var device_id = window.localStorage.getItem("device_id");
    $.each(capa, function(index,value){
        $.post({
            data: {"device_id":device_id, "capa_id":value},
            url: "http://54.91.87.65/api_test_on_board/public/index.php/capa",
            async: true,
            success: function(result) {
                var carros_marker = [];
                window.localStorage.setItem("capas", result);
                var capa = JSON.parse(result);
                var element = document.createElement("button");
                element.type = "button";
                element.id = capa.name;
                var nuevaDiv = document.getElementById("capas");
                nuevaDiv.appendChild(element);
                $("#" + capa.name).html(capa.name);
                $("#" + capa.name).css("display","none");
                $("#" + capa.name).addClass("btn btn-danger btn-md col-xs-12");
                $("#" + capa.name).click(function(){
                    var clicked = $(this).data('clicked');
                    if ( clicked ) {
                        setMapOnAll(null, carros_marker);
                    }else{
                        var i=0;
                        $.each(capa.data,function(index,value){
                             var carro_latlon = {"lat":value.lat , "lng":value.lon};
                             var carro = new google.maps.Marker({position: carro_latlon});
                             carros_marker.push(carro);
                        });
                        setMapOnAll(map, carros_marker);
                    }
                    $(this).data('clicked', !clicked);    
                });
            },
            error: function(request, error) {
                var texto = "Error conectando con el servidor!";
                console.log(error);
            }
        });
    });
}



function display_c(){  //prints date and hour
    var refresh=1000; // Refresh rate in milli seconds
    mytime=setTimeout('display_ct()',refresh)
}

function display_ct() {
    var strcount
    var x = new Date()
    var x1= ('0' + x.getDate()).slice(-2) + "/" + ('0' + (x.getMonth()+1)).slice(-2) + "/" + x.getFullYear() + " " +  ('0' + x.getHours( )).slice(-2)+ ":" + ('0' + x.getMinutes()).slice(-2) + ":" + ('0' + x.getSeconds()).slice(-2);
   
    x1 = x1;
    document.getElementById('hora').innerHTML = x1;
    
    display_c();
}