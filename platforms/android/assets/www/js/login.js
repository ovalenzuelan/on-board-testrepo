    var $formLogin = $('#login-form');
    var $divForms = $('#div-forms');
    var $modalAnimateTime = 300;
    var $msgAnimateTime = 150;
    var $msgShowTime = 4000;

    $("#login-modal").modal({   //evita salir del login presionando fuera del formulario.
        backdrop: 'static',
        show: 'false'
    })

    $("#login-form").submit(function(event) {
    switch (this.id) {
        case "login-form":
            
            event.preventDefault();
            var $lg_username = $('#login_username').val();
            var $lg_password = $('#login_password').val();
            

            $.post({
                url: 'http://54.91.87.65/api_test_on_board/public/index.php/login',
                data: {"username": $lg_username , "password": $lg_password},
                dataType: "JSON",
                async: true,
                success: function(result) {
                    console.log(result);
                    window.localStorage.setItem("device_id",result.device_id);
                    $("#login-modal").modal("hide");
                    llamaBomberos();
                },
                error: function(request, error) {
                    console.log(error);
                    $('#login_username').val("");
                    $('#login_password').val("");
                    msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "error", "glyphicon-remove", "Usuario y/o contraseña incorrecto");
                }
            });
    }
});

    function modalAnimate($oldForm, $newForm) {
        var $oldH = $oldForm.height();
        var $newH = $newForm.height();
        $divForms.css("height", $oldH);
        $oldForm.fadeToggle($modalAnimateTime, function() {
            $divForms.animate({
                height: $newH
            }, $modalAnimateTime, function() {
                $newForm.fadeToggle($modalAnimateTime);
            });
        });
    }

    function msgFade($msgId, $msgText) {
        $msgId.fadeOut($msgAnimateTime, function() {
            $(this).text($msgText).fadeIn($msgAnimateTime);
        });
    }

    function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) {
        var $msgOld = $divTag.text();
        msgFade($textTag, $msgText);
        $divTag.addClass($divClass);
        $iconTag.removeClass("glyphicon-chevron-right");
        $iconTag.addClass($iconClass + " " + $divClass);
        setTimeout(function() {
            msgFade($textTag, $msgOld);
            $divTag.removeClass($divClass);
            $iconTag.addClass("glyphicon-chevron-right");
            $iconTag.removeClass($iconClass + " " + $divClass);
        }, $msgShowTime);
    }


    $("#cerrarSesion").click(function(){
        window.localStorage.removeItem("device_id");
        $("#login-modal").modal("show");
    });

    function verifyToken(){ 
        console.log("verificando dispositivo");
        if (window.localStorage.getItem("device_id") == "B1") {
            console.log("Bienvenido de vuelta a OnBoard");
            $("#login-modal").modal("hide");
        }else{
            console.log("sesion no inciada");
            $("#login-modal").modal("show");
        }
    }